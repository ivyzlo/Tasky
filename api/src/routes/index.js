const express = require ("express");
const router = express.Router();

const userRoutes = require("./../domains/user");
const OTPRoutes = require ("./../domains/otp");
const EmailVerificationRoutes = require("./../domains/email_verification");
const ForgotPasswordRoutes = require("./../domains/forgot_password");
const ServiceRoutes = require("./../domains/service");
const Demande=require("./../domains/demande");
const Conversation=require("./../domains/chat/conversation");
const Message=require("./../domains/chat/message");



router.use("/user", userRoutes);
router.use("/otp", OTPRoutes);
router.use("/email_verification",EmailVerificationRoutes);
router.use("/forgot_password", ForgotPasswordRoutes);
router.use("/service",ServiceRoutes);
router.use("/demand",Demande);
router.use("/Conversation",Conversation);
router.use("/Message",Message);
module.exports = router ;