const User = require("./model");
const{hashData,verifyHashedData} = require("./../../util/hashData");
const createToken=require("./../../util/createToken");
const authenticateUser = async (data) => {
    try {
        const  {email,password} = data;
        const fetchedUser = await User.findOne({email});

        if (!fetchedUser){
            throw Error("Invalid credentials entered !! try again");
        }
        if(!fetchedUser.verified){
            throw Error ("email isn't verified yet . check inbox .");
        }

        const hashedPassword = fetchedUser.password;
        const passwordMatch = await verifyHashedData(password,hashedPassword);
         if (!passwordMatch){
            throw Error ("invalid password entered!");
        }
          
        // create user token 
        const tokenData = {userId:fetchedUser._id,email};
        const token = await createToken(tokenData);

        // assign user token 
        fetchedUser.token=token;
        return fetchedUser;
    } catch (error) {
        throw error;
    }
}



const createNewUser = async (data) => {
 try {
  const { name,email,phone, password , role } = data ;
   // checking if user already exists
   const existingUser = await User.findOne({email});
    if (existingUser) {
        throw Error("User with this email already exists");
    }else{
        const hashedPassword = await hashData(password);
        const newUser = new User ({
         name,
         email,
         phone,
         password:hashedPassword,
         role,
        })
        // save user
        const createdUser = await newUser.save();
        return createdUser;     
    }
    // hash password 
      
 } catch (error) {
    throw error;
 }

};
module.exports ={createNewUser, authenticateUser};