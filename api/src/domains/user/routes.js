const express = require ("express");
const router=express.Router();
const { createNewUser , authenticateUser} = require("./controller");
const auth=require("./../../middelware/auth");
const {sendVerificationOTPEmail}= require("./../email_verification/controller");

// protected route 
router.get("/private_data", auth,(req,res) => {
 res.status(200).send(`You're in the private territory of ${req.currentUser.email}`);
})


// Signin
 router.post("/Signin", async (req,res) => {
    try {
        let {email,password} = req.body;
        email=email.trim();
        password = password.trim();
        if (!(email && password)) {
            throw Error ("Empty credentials!");
        }

       const authenticatedUser = await authenticateUser({email,password});
       res.status(200).json(authenticatedUser);
    } catch (error) {
        res.status(400).send(error.message);
    }
 }
 
 )



// Signup 
router.post("/Signup", async (req,res)=>{
 try {
    let { name,email,phone,password,role } =req.body;
    name = name.trim();
    phone=phone.trim();
    email=email.trim();
    role=role.trim();
    password=password.trim();

    
    if (!(name && phone && email && password  )){
    throw Error("Empty input fields");
    } else if (!/^[a-zA-Z ]*$/.test(name)) {
        throw Error("Invalid name entered");
    }else if (!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)){
        throw Error ("Invalid email entered");
    } else if (password.length < 8)  {
        throw Error ("Password is has to have 8 letters at least or it's not similar to password check ");
    }else {
        // create new user field 
        const newUser = await createNewUser({
            name,
            email,
            phone,
            password,
            role,
        })

        res.status(200).json({
            status: "success",
            message: newUser});
        await sendVerificationOTPEmail(email);

    }
 } catch (error) {
    res.status(400).json({
        status: "Failed",
        message : error.message
    });
 }
})
module.exports = router;