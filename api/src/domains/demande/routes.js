const express = require('express');
const admin = require('firebase-admin');
const demande=require("./../demande/model");
const User=require("./../user/model");
const Service=require("./../service/model");
const router = express.Router();

// Initialize Firebase Admin SDK
const serviceAccount = require('./../firebasekey/privatekey.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://Tasky.firebaseio.com'
});
// Send a demand for a service
router.post('/demand', async (req, res) => {
  const { description, Serviceid, status } = req.body;

  const demand = new demande({
    description,
    Serviceid,
    status
  });

  try {
    const newDemand = await demand.save();

    // Send push notification to the client
    const service = await Service.findById(Serviceid).populate('userid');
    const userid = service.userid;
    const user = await User.findById(userid);

    const message = {
      notification: {
        title: 'New Service Demand',
        body: `New demand for the service you published '${service.description}'`
      },
      data: {
        demandId: newDemand._id.toString()
      },
      token: user.token
    };

    await admin.messaging().send(message);
    res.status(200).send(newDemand);
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
});

// Accept or refuse a demand
router.put('/demand/:id', async (req, res) => {
  const { status } = req.body;

  try {
    const demand = await demand.findById(req.params.id);
    if (!demand) {
      return res.status(404).send('Demand not found');
    }

    demand.status = status;
    const updatedDemand = await demand.save();

    // Send push notification to the provider who made the demand
    const user = await User.findById(demand.userId);
    const message = {
      notification: {
        title: `Demand ${status}`,
        body: `Your demand for '${demand.service.description}' has been ${status}`
      },
      data: {
        demandId: updatedDemand._id.toString()
      },
      token: user.token
    };

    await admin.messaging().send(message);

    res.status(200).send(updatedDemand);
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
});

module.exports = router;
