const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const demandeSchema = new Schema({
     Serviceid : {type:String,required:true},
     description: {type: String, required: true},
     status: { type: String, enum: ['pending', 'accepted', 'refused'], default: 'pending' },
});

const Service = mongoose.model("demande",demandeSchema);

module.exports = Service;