const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
     conversationId : {type:String},
     Sender: {type: String},
     text: {type: String},
});

const Message = mongoose.model("Message",MessageSchema);

module.exports = Message;