const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ConversationSchema = new Schema({
     members :Array,
});

const Conversation = mongoose.model("Conversation",ConversationSchema);

module.exports = Conversation;