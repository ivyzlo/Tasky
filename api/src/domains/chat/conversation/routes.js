const express = require('express');
const router = express.Router();
const Conversation = require("../conversation/model");

//new conv
router.post("/", async (req,res)=>{
    const newConversation = new Conversation({
        membres:[req.body.senderId,req.body.receiverId],
        });
 try {
    const savedConversation = await newConversation.save();
    res.status(200).json(savedConversation);
 } catch (error) {
    res.status(500).json(error);
 }
})
//get conv

router.get("/:userid", async(req,res)=>{

try {
    const conversation = await Conversation.find({
        members : {$in:[req.params.userid]},
    })
    res.status(200).json(conversation);
} catch (error) {
    res.status(500).json(error);
}




})
module.exports = router;