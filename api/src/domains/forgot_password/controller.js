const User= require("./../user/model");
const{sendOTP,verifyOTP,deleteOTP}= require("./../otp/controller");
const{hashData}= require("./../../util/hashData");

const resetUserPassword = async ({email,otp,newPassword}) =>{
    try {
        const validOTP=await verifyOTP({email,otp});
       if (!validOTP){
        throw Error("invalid code check inbox.");
       }   

       // update new pass
       if (newPassword.length< 8 ){
         throw Error("password too short");
       }
   const hashedNewPassword= await hashData(newPassword);
   await User.updateOne({email},{password:hashedNewPassword});
     await deleteOTP(email);
     return;

} catch (error) {
        throw error;
    }
}   



const sendPasswordResetOTPEmail=async(email) =>{
    try {
        // account exists?
       const existingUser= await User.findOne({email});
       if (!existingUser){
        throw Error("there's no account for the provided email.");

       }
       if (!existingUser.verified){
        throw Error("email nit verified.check inbox");
       }

        const otpDetails={
            email,
            subject:"Password Reset",
            message:"Enter the code below to reset your email.",
            duration:1,}
       const createdOTP= await sendOTP(otpDetails);
       return createdOTP;

    } catch (error) {
        throw error;
    }
}
module.exports ={sendPasswordResetOTPEmail,resetUserPassword};