const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ServiceSchema = new Schema({
     userid : {type:String,required:true},
     description: {type: String, required: true,max:500},
     Type: String,
     localisation: String,
     price: String,
     status: { type: String, enum: ['request', 'no request'], default: 'no requests' },
});

const Service = mongoose.model("Service",ServiceSchema);

module.exports = Service;