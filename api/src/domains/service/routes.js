const express= require("express");
const router = express.Router();
const Service=require("./../service/model");
const User = require("../user/model");


// create a post
router.get("/", async (req,res)=>{
    const newService = new Service(req.body)
    try {
           const savedService = await newService.save();
           res.status(200).json(savedService);
    } catch (error) {
       res.status(400).json({
           status: "Failed",
           message : error.message
       });
    }
   })
//update a service
router.put("/:id", async(req,res) =>{
    try{
    const service = await Service.findById(req.params.id);
    if(service.userid === req.body.userid){
       await service.updateOne({$set:req.body});
       res.status(200).json("the post has been updated");
    }else{
        res.status(403).json("you can update only your post");
    }
    }catch(error){
        res.status(500).json(error);
    }
})
//delete a service
router.delete("/:id", async(req,res) =>{
    try{
    const service = await Service.findById(req.params.id);
    if(service.userid === req.body.userid){
       await service.deleteOne();
       res.status(200).json("the post has been deleted");
    }else{
        res.status(403).json("you can delete only your post");
    }
    }catch(error){
        res.status(500).json(error);
    }
})

// show all your previous posts

router.get("/history",async(req,res) =>{
    
    try {
        const currentUser = await User.findById(req.body.userid);
        const userServices = await Service.find({userid:currentUser._id});

        res.json(userServices);
    } catch (error) {
        res.status(500).json("you don't have any services");
        
    }
})
module.exports=router;